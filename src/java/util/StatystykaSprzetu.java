/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import model.Czynnosc;

/**
 *
 * @author Piotrek
 */
public class StatystykaSprzetu {
    private Czynnosc czynnosc;
    private double koszt;
    private int czas;

    public Czynnosc getCzynnosc() {
        return czynnosc;
    }

    public void setCzynnosc(Czynnosc czynnosc) {
        this.czynnosc = czynnosc;
    }

    public double getKoszt() {
        return koszt;
    }

    public void setKoszt(double koszt) {
        this.koszt = koszt;
    }

    public int getCzas() {
        return czas;
    }

    public void setCzas(int czas) {
        this.czas = czas;
    }
    
}
