/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Piotrek
 */
@Entity
@Table(name = "USERS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u"),
    @NamedQuery(name = "Users.findByUsername", query = "SELECT u FROM Users u WHERE u.username = :username"),
    @NamedQuery(name = "Users.findByPassword", query = "SELECT u FROM Users u WHERE u.password = :password")})
public class Users implements Serializable {
    @OneToMany(mappedBy = "odbiorca")
    private List<Wiadomosc> odebraneList;
    @OneToMany(mappedBy = "nadawca")
    private List<Wiadomosc> wyslaneList;
    @OneToMany(mappedBy = "klient")
    private List<Zadanie> zadanieList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "USERNAME")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "PASSWORD")
    private String password;

    public Users() {
    }

    public Users(String username) {
        this.username = username;
    }

    public Users(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (username != null ? username.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.username == null && other.username != null) || (this.username != null && !this.username.equals(other.username))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Users[ username=" + username + " ]";
    }

    @XmlTransient
    public List<Zadanie> getZadanieList() {
        return zadanieList;
    }

    public void setZadanieList(List<Zadanie> zadanieList) {
        this.zadanieList = zadanieList;
    }

    @XmlTransient
    public List<Wiadomosc> getOdebraneList() {
        return odebraneList;
    }

    public void setOdebraneList(List<Wiadomosc> odebraneList) {
        this.odebraneList = odebraneList;
    }

    @XmlTransient
    public List<Wiadomosc> getWyslaneList() {
        return wyslaneList;
    }

    public void setWyslaneList(List<Wiadomosc> wyslaneList) {
        this.wyslaneList = wyslaneList;
    }
    
}
