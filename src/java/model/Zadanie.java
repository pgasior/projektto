/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Piotrek
 */
@Entity
@Table(name = "ZADANIE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Zadanie.findAll", query = "SELECT z FROM Zadanie z"),
    @NamedQuery(name = "Zadanie.findById", query = "SELECT z FROM Zadanie z WHERE z.id = :id"),
    @NamedQuery(name = "Zadanie.findByNazwa", query = "SELECT z FROM Zadanie z WHERE z.nazwa = :nazwa"),
    @NamedQuery(name = "Zadanie.findByCzasRozpoczecia", query = "SELECT z FROM Zadanie z WHERE z.czasRozpoczecia = :czasRozpoczecia"),
    @NamedQuery(name = "Zadanie.findByKlient", query = "SELECT z FROM Zadanie z WHERE z.klient.username = :username")
})
public class Zadanie implements Serializable {
    @Column(name = "STATUS")
    private Integer status;
    @JoinColumn(name = "KLIENT", referencedColumnName = "USERNAME")
    @ManyToOne
    private Users klient;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NAZWA")
    private String nazwa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CZAS_ROZPOCZECIA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date czasRozpoczecia;
    @OneToMany(mappedBy = "idZadanie")
    private List<Czynnosc> czynnoscList;

    public Zadanie() {
    }

    public Zadanie(Integer id) {
        this.id = id;
    }

    public Zadanie(Integer id, String nazwa, Date czasRozpoczecia) {
        this.id = id;
        this.nazwa = nazwa;
        this.czasRozpoczecia = czasRozpoczecia;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public Date getCzasRozpoczecia() {
        return czasRozpoczecia;
    }

    public void setCzasRozpoczecia(Date czasRozpoczecia) {
        this.czasRozpoczecia = czasRozpoczecia;
    }

    @XmlTransient
    public List<Czynnosc> getCzynnoscList() {
        return czynnoscList;
    }

    public void setCzynnoscList(List<Czynnosc> czynnoscList) {
        this.czynnoscList = czynnoscList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Zadanie)) {
            return false;
        }
        Zadanie other = (Zadanie) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Zadanie[ id=" + id + " ]";
    }

    public Users getKlient() {
        return klient;
    }

    public void setKlient(Users klient) {
        this.klient = klient;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    
    
    
}
