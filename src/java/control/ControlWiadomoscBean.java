/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import ejb.DataBean;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import model.Users;
import model.Wiadomosc;
import org.primefaces.context.RequestContext;


/**
 *
 * @author Piotrek
 */
@ManagedBean(name = "controlWiadomosc")
@RequestScoped
public class ControlWiadomoscBean {

    @EJB
    private DataBean db;
    private String wiadomosc;
    private List<Users> klienci;
    private List<Users> uzytkownicy;
    private List<Wiadomosc> wiadomosci;
    private String odbiorca;
    private String tytul;
    private Wiadomosc wybranaWiadomosc;
    public ControlWiadomoscBean() {
    }

    public String getWiadomosc() {
        return wiadomosc;
    }

    public void setWiadomosc(String wiadomosc) {
        this.wiadomosc = wiadomosc;
    }

    public List<Users> getKlienci() {
        klienci = db.getUsersFromGroup("klient");
        return klienci;
    }

    public void setKlienci(List<Users> klienci) {
        this.klienci = klienci;
    }

    public List<Users> getUzytkownicy() {
        uzytkownicy = db.getUsersFromGroup("uzytkownik");
        return uzytkownicy;
    }

    public void setUzytkownicy(List<Users> uzytkownicy) {
        this.uzytkownicy = uzytkownicy;
    }

    public List<Wiadomosc> getWiadomosci() {
        wiadomosci = db.getOdebraneWiadomosci(FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName());
        return wiadomosci;
    }

    public void setWiadomosci(List<Wiadomosc> wiadomosci) {
        this.wiadomosci = wiadomosci;
    }

    public String getOdbiorca() {
        return odbiorca;
    }

    public void setOdbiorca(String odbiorca) {
        this.odbiorca = odbiorca;
    }
    
    public void wyslij() {
        String nadawca = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        db.wyslijWiadomosc(this.wiadomosc,this.tytul,this.odbiorca,nadawca);
        this.wiadomosc = "";
        this.tytul = "";
        RequestContext.getCurrentInstance().execute("PF('dlg').hide()");
    }

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }
    
    public void odczytaj(Wiadomosc w) {
        this.wybranaWiadomosc = w;
        db.setWiadomoscPrzeczytana(w.getId());
        RequestContext.getCurrentInstance().execute("PF('dlg_read').show()");
        
    }

    public Wiadomosc getWybranaWiadomosc() {
        return wybranaWiadomosc;
    }

    public void setWybranaWiadomosc(Wiadomosc wybranaWiadomosc) {
        this.wybranaWiadomosc = wybranaWiadomosc;
    }

    
}
