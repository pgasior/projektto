/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import ejb.DataBean;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import model.Zastosowanie;

/**
 *
 * @author Piotrek
 */
@ManagedBean(name = "controlZastosowanie")
@RequestScoped
public class ControlZastosowanieBean {

    
    private static Logger logger = Logger.getLogger(".control.ControlZastosowanieBean");
    
    private List<Zastosowanie> Zastosowania;
    
    @EJB
    private DataBean db;
    /**
     * Creates a new instance of ControlZastosowanieBean
     */
    public ControlZastosowanieBean() {
    }
    
    public List<Zastosowanie> getZastosowania() {
        try{
            this.Zastosowania = db.getZastosowania();
        } catch(Exception e) {
            logger.warning(("nieudalo sie pobrac zastosowan"));
            e.printStackTrace();
        }
        return Zastosowania;
    }
    
    public void deleteZastosowanie(Zastosowanie zastosowanie) {
        try {
            db.deleteZastosowanie(zastosowanie);
        } catch (Exception e) {
            logger.warning(e.getMessage());
            e.printStackTrace();
        }
    }
    
}
