/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import ejb.DataBean;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Przedmiot;
import model.Zastosowanie;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Piotrek
 */
@ManagedBean(name = "addZastosowanieForPrzedmiot")
@ViewScoped
public class AddZastosowanieForPrzedmiotBean {

    /**
     * Creates a new instance of AddZastosowanieForPrzedmiotBean
     */
    
    private int przedmiotID;
    private Przedmiot przedmiot;
    
    private DualListModel<Zastosowanie> zastosowania;
    
    @EJB
    private DataBean db;
    
    public AddZastosowanieForPrzedmiotBean() {
    }

    /**
     * @return the przedmiotID
     */
    public int getPrzedmiotID() {
        return przedmiotID;
    }

    /**
     * @param przedmiotID the przedmiotID to set
     */
    public void setPrzedmiotID(int przedmiotID) {
        this.przedmiotID = przedmiotID;
        this.przedmiot = db.getPrzedmiotById(przedmiotID);
        //List<Zastosowanie> = db.get
    }

    /**
     * @return the przedmiot
     */
    public Przedmiot getPrzedmiot() {
        return przedmiot;
    }

    /**
     * @param przedmiot the przedmiot to set
     */
    public void setPrzedmiot(Przedmiot przedmiot) {
        this.przedmiot = przedmiot;
    }

    /**
     * @return the zastosowania
     */
    public DualListModel<Zastosowanie> getZastosowania() {
        List<Zastosowanie> dostepne_zast = db.getNieprzypisaneZastosowaniaPrzedmiotu(this.przedmiotID);
        List<Zastosowanie> przypisane_zast= this.przedmiot.getZastosowanieList();
        this.zastosowania = new DualListModel<>(dostepne_zast,przypisane_zast);
        return zastosowania;
    }
    
    public void add_zastosowania() {
        db.addZastosowaniaPrzedmiotu(this.przedmiotID,zastosowania);
    }

    /**
     * @param zastosowania the zastosowania to set
     */
    public void setZastosowania(DualListModel<Zastosowanie> zastosowania) {
        this.zastosowania = zastosowania;
    }


    
}
