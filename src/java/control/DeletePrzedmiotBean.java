/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import ejb.DataBean;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import model.Przedmiot;

/**
 *
 * @author Piotrek
 */
@ManagedBean(name = "deletePrzedmiot")
@RequestScoped
public class DeletePrzedmiotBean {

    /**
     * Creates a new instance of DeletePrzedmiotBean
     */
    
    private static Logger logger = Logger.getLogger(".controll.DeletePrzedmiotBean");

    @EJB
    private DataBean db;
    public DeletePrzedmiotBean() {
    }
    
    public void deletePrzedmiot(Przedmiot przedmiot) {
        try {
            db.deletePrzedmiot(przedmiot);
        } catch (Exception e) {
            logger.warning(e.getMessage());
            e.printStackTrace();
        }
    }
    
}
