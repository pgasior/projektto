/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import ejb.DataBean;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import model.Pracownik;
import model.Przedmiot;
import model.Zastosowanie;

/**
 *
 * @author Piotrek
 */
@ManagedBean(name = "controlZastosowaniaPrzedmiotuPracownika")
@ViewScoped
public class ControlZastosowaniaPrzedmiotuPracownikaBean {

    private static Logger logger = Logger.getLogger(".control.ControlZastosowaniaPrzedmiotuPracownikaBean");
    
    @EJB
    private DataBean db;
    
    private int pracownikID;
    private int przedmiotID;
    private Przedmiot przedmiot;
    private Pracownik pracownik;
    private List<Zastosowanie> zastosowania;
    /**
     * Creates a new instance of ControlZastosowaniaPrzedmiotuPracownikaBean
     */
    public ControlZastosowaniaPrzedmiotuPracownikaBean() {
    }

    /**
     * @return the pracownikID
     */
    public int getPracownikID() {
        return pracownikID;
    }

    /**
     * @param pracownikID the pracownikID to set
     */
    public void setPracownikID(int pracownikID) {
        this.pracownikID = pracownikID;
        this.pracownik = db.getPracownikById(pracownikID);
    }
    
    public String imiePracownika() {
        return this.pracownik.getImie()+" "+this.pracownik.getNazwisko();
    }
    
    public void deleteZastosowanie(Zastosowanie zastosowanie) {
        this.db.removeZastosowaniePrzedmiotu(zastosowanie.getId(),this.przedmiotID);
    }

    /**
     * @return the przedmiotID
     */
    public int getPrzedmiotID() {
        return przedmiotID;
    }

    /**
     * @param przedmiotID the przedmiotID to set
     */
    public void setPrzedmiotID(int przedmiotID) {
        this.przedmiotID = przedmiotID;
        this.setPrzedmiot(db.getPrzedmiotById(przedmiotID));
    }

    /**
     * @return the zastosowania
     */
    public List<Zastosowanie> getZastosowania() {
        this.zastosowania = db.getZastosowaniaPrzedmiotu(this.przedmiotID);
        return zastosowania;
    }

    /**
     * @return the pracownik
     */
    public Pracownik getPracownik() {
        return pracownik;
    }

    /**
     * @param pracownik the pracownik to set
     */
    public void setPracownik(Pracownik pracownik) {
        this.pracownik = pracownik;
    }

    /**
     * @return the przedmiot
     */
    public Przedmiot getPrzedmiot() {
        return przedmiot;
    }

    /**
     * @param przedmiot the przedmiot to set
     */
    public void setPrzedmiot(Przedmiot przedmiot) {
        this.przedmiot = przedmiot;
    }
    
}
