/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import ejb.DataBean;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import model.Pracownik;
import model.Przedmiot;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Piotrek
 */
@ManagedBean(name = "AddPrzedmiotForPracownik")
@RequestScoped
public class ControllAddPrzedmiotForPracownik {

    /**
     * Creates a new instance of ControllAddPrzedmiotForPracownik
     */
    
    private static Logger logger = Logger.getLogger(".control.ControllAddPrzedmiotForPracownik");
    @ManagedProperty(value="#{param.pracid}")
    private int pracownikID;
    
    @EJB
    private DataBean db;
    
    private DualListModel<Przedmiot> przedmioty;
    private Pracownik pracownik;
    
    @PostConstruct
    public void init() {
        List<Przedmiot> przedmiotySource= db.getWolnePrzedmioty();
        
        List<Przedmiot> przedmiotyTarget = db.getPrzedmiotyPracownika(this.pracownikID);
        
        przedmioty = new DualListModel<>(przedmiotySource,przedmiotyTarget);
        pracownik = db.getPracownikById(this.pracownikID);
        
    }
    
    public void addPrzedmioty() {
        db.addPrzedmiotForPracownik(pracownik, przedmioty);
    }
    
    public ControllAddPrzedmiotForPracownik() {
    }

    /**
     * @return the pracownikID
     */
    public int getPracownikID() {
        return pracownikID;
    }

    /**
     * @param pracownikID the pracownikID to set
     */
    public void setPracownikID(int pracownikID) {
        this.pracownikID = pracownikID;
    }

    /**
     * @return the przedmioty
     */
    public DualListModel<Przedmiot> getPrzedmioty() {
        return przedmioty;
    }

    /**
     * @param przedmioty the przedmioty to set
     */
    public void setPrzedmioty(DualListModel<Przedmiot> przedmioty) {
        this.przedmioty = przedmioty;
    }

    /**
     * @return the pracownik
     */
    public Pracownik getPracownik() {
        return pracownik;
    }

    /**
     * @param pracownik the pracownik to set
     */
    public void setPracownik(Pracownik pracownik) {
        this.pracownik = pracownik;
    }
    
}
