/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import ejb.DataBean;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Piotrek
 */
@ManagedBean(name = "AddPrzedmiot")
@RequestScoped
public class AddPrzedmiotBean {

    /**
     * Creates a new instance of AddPrzedmiotBean
     */
    private static Logger logger = Logger.getLogger(".controll.AddPrzedmiotBean");

    @EJB
    private DataBean db;

    private String nazwaNewP;

    public AddPrzedmiotBean() {

    }

    /**
     * @return the newNazwaP
     */
    public String getNazwaNewP() {
        return nazwaNewP;
    }

    /**
     * @param nazwaNewP the newNazwaP to set
     */
    public void setNazwaNewP(String nazwaNewP) {
        this.nazwaNewP = nazwaNewP;
    }

    public void addNewPrzedmiot() {
        try {
            db.addPrzedmiot(nazwaNewP);
            this.addMessage("Dodano przedmiot: " + nazwaNewP);
            this.nazwaNewP = "";

        } catch (Exception e) {
            logger.warning(e.getMessage());
            e.printStackTrace();
        }
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

}
