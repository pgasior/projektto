/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import ejb.DataBean;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import model.Czynnosc;
import model.Pracownik;
import model.Zadanie;
import util.StatystykaPracownika;
import util.StatystykaSprzetu;

/**
 *
 * @author Piotrek
 */
@ManagedBean(name = "controlZadanie")
@RequestScoped
public class ControlZadanieBean {

    /**
     * Creates a new instance of ControlZadanieBean
     */
    
    private static Logger logger = Logger.getLogger(".control.ControlZadanieBean");
    
    private List<Zadanie> zadania;
    private List<Zadanie> wybraneZadania = new ArrayList<>();
    
    @EJB
    private DataBean db;
    
    public ControlZadanieBean() {
    }
    
    public List<Zadanie> getZadaniaKlienta() {
        String username = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        List<Zadanie> zad = db.getZadaniaKlienta(username);
        return zad;
    }

    public List<Zadanie> getZadania() {
        this.zadania = this.db.getZadania();
        return zadania;
    }
    
    public void deleteZadanie(Zadanie zadanie) {
        this.db.removeZadanie(zadanie.getId());
    }

    public void setZadania(List<Zadanie> zadania) {
        this.zadania = zadania;
    }

    public List<Zadanie> getWybraneZadania() {
        return wybraneZadania;
    }

    public void setWybraneZadania(List<Zadanie> wybraneZadania) {
        this.wybraneZadania = wybraneZadania;
    }
    
    
    
    public int czasTrwaniaZadania(Zadanie zadanie) {
        int czasTrwania = 0;
        for(Czynnosc c : zadanie.getCzynnoscList()) {
            czasTrwania+=c.getCzasTrwania();
        }
        return czasTrwania;
    }
    
    public Date dataZakonczeniaZadania(Zadanie zadanie) {
        int czasTrwania = this.czasTrwaniaZadania(zadanie);
        Calendar cal = Calendar.getInstance();
        cal.setTime(zadanie.getCzasRozpoczecia());
        cal.add(Calendar.SECOND, czasTrwania);
        Date czasZakonczenia = cal.getTime();
        return czasZakonczenia;
    }
    
    

    private double kosztPracownikow(List<Pracownik> pracownikList, int czas) {
        double ile_minut = (double)czas/(double)pracownikList.size()/60.0;
        double koszt = 0.0;
        for(Pracownik p : pracownikList) {
            koszt+=p.getStawka()*ile_minut;
        }
        return koszt;
        
    }
    
    public List<Pracownik> getPracownikList(Zadanie zadanie) {
        HashSet<Pracownik> pracownicy;
        pracownicy = new HashSet<>();
        for(Czynnosc c : zadanie.getCzynnoscList()) {
            for(Pracownik p : c.getPracownikList()) {
                pracownicy.add(p);
            }
        }
        List<Pracownik> pracownikList = new ArrayList<>(pracownicy);
        return pracownikList;
   
    }
    
    public List<StatystykaPracownika> statystykaPracownika(Zadanie z) {
        int czas = 0;
        ArrayList<StatystykaPracownika> lsp = new ArrayList<>();
        for(Pracownik p : this.getPracownikList(z)) {
            StatystykaPracownika sp = new StatystykaPracownika();
            sp.setPracownik(p);
            for(Czynnosc c : p.getCzynnoscList()) {
                if(z.getId()==c.getIdZadanie().getId()) {
                    int ilep = c.getPracownikList().size();
                    sp.setCzas(sp.getCzas() + (double)c.getCzasTrwania()/(double)ilep);
                    sp.setKoszt(sp.getKoszt() + (double)c.getCzasTrwania()/60.0*p.getStawka()/(double)ilep);
                    
                }
            }
            lsp.add(sp);
        }
        return lsp;
    }
    
    public List<StatystykaSprzetu> statystykaSprzetu(Zadanie z) {
        ArrayList<StatystykaSprzetu> lsp = new ArrayList<>();
        for(Czynnosc c : z.getCzynnoscList()) {
            StatystykaSprzetu sp = new StatystykaSprzetu();
            sp.setCzynnosc(c);
            sp.setCzas(c.getCzasTrwania());
            sp.setKoszt(c.getCzasTrwania()/60.0*c.getStawka());
            lsp.add(sp);
        }
        return lsp;
    }
    
    public double kosztZadania(Zadanie zadanie) {
        double koszt = 0;
        for(Czynnosc c : zadanie.getCzynnoscList()) {
            koszt+=this.kosztCzynnosci(c);
        }
        return koszt;
    }
    
    public double kosztWieluZadan(List<Zadanie> zadania) {
        double koszt = 0;
        for(Zadanie z : zadania) {
            koszt+=this.kosztZadania(z);
        }
        return koszt;
    }
    
    public double kosztCzynnosci(Czynnosc c) {
        double koszt = 0;
        
        int ilePrac = c.getPracownikList().size();
        if(ilePrac==0)
            return 0.0;
        koszt+=c.getStawka()*c.getCzasTrwania()/60.0;
        for(Pracownik p : c.getPracownikList()) {
            koszt+=this.kosztPracownika(p, (double)c.getCzasTrwania()/(double)ilePrac);
        }
        return koszt;
    }
    
    public double kosztPracownika(Pracownik p, double czas) {
        return p.getStawka()*czas;
    }
    
    public double sredniaStawkaPracownika(Zadanie z) {
        double srednia = 0.0;
        List<StatystykaPracownika> lsp = this.statystykaPracownika(z);
        for(StatystykaPracownika sp : lsp) {
            srednia += sp.getKoszt();
        }
        srednia/=(double)lsp.size();
        return srednia;
    }
    
    public double sredniKosztCzynnosci(Zadanie z) {
        double srednia = 0.0;
        List<StatystykaSprzetu> lsp = this.statystykaSprzetu(z);
        for(StatystykaSprzetu sp : lsp) {
            srednia += sp.getKoszt();
        }
        srednia/=(double)lsp.size();
        return srednia;
    }
    
    public String statusZadania(Zadanie z) {
        if(z.getStatus()==0) {
            return "Oczekuje na wykonanie";
        } else if(z.getStatus()==1) {
            return "W trakcie wykonania";
        } else {
            return "Zakonczone";
        }
    }
    
    public void zatwierdzZadanie(Zadanie z) {
        if(z.getStatus()==2) {
            db.removeZadanie(z.getId());
        }
    }
    
}
