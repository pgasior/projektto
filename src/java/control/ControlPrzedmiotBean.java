/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import ejb.DataBean;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import model.Przedmiot;

/**
 *
 * @author Piotrek
 */
@ManagedBean(name = "controlPrzedmiot")
@RequestScoped
public class ControlPrzedmiotBean {

    /**
     * Creates a new instance of ControlPrzedmiotBean
     */
    private static Logger logger = Logger.getLogger(".control.ControlPrzedmiotBean");

    @EJB
    private DataBean db;
    
    private List<Przedmiot> Przedmioty;

    public ControlPrzedmiotBean() {
    }

    public List<Przedmiot> getPrzedmioty() {
        try {
            this.Przedmioty = db.getPrzedmioty();
        } catch (Exception e) {
            logger.warning(("nieudalo sie pobrac przedmiotow"));
            e.printStackTrace();
        }
        return Przedmioty;
    }

    public String ownerName(Przedmiot przedmiot) {
        if (przedmiot.getIdPracownik() != null) {
            return przedmiot.getIdPracownik().getImie() + " " + przedmiot.getIdPracownik().getNazwisko();
        }
        return "";
    }

}
