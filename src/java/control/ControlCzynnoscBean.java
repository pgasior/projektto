/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import ejb.DataBean;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import model.Czynnosc;
import model.Pracownik;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Piotrek
 */
@ManagedBean(name = "controlCzynnosc")
@ViewScoped
public class ControlCzynnoscBean {
    
    private static Logger logger = Logger.getLogger(".controll.ControlCzynnoscBean");
    
    private DualListModel<Pracownik> pracownicy = new DualListModel<>();
    
    @EJB
    private DataBean db;

    private int idZadanie;
    
    private List<Czynnosc> czynnoscList = null;
    
    private String tmpczynnoscNazwa;
    private Double tmpczynnoscStawka;
    private int tmpczynnoscCzasTrwania;
    
    
    public ControlCzynnoscBean() {
    }



    public List<Czynnosc> getCzynnoscList() {
        if(this.czynnoscList==null) {
            this.czynnoscList = db.getCzynnoscForZadanie(this.idZadanie);
            
        }
        return czynnoscList;
    }
    
    public DualListModel<Pracownik> getPracownicy() {
        //List<Pracownik> prac= db.getPracownicyPosadajacyOdpowiednieNarzedzia(this.czynnoscList);
        //List<Pracownik> pusta = new ArrayList<>();
        //pracownicy = new DualListModel<>(prac, pusta);
        return pracownicy;
    }
    
    public void deleteCzynnosc(Czynnosc czynnosc) {
        
        this.czynnoscList.remove(czynnosc);
        db.removeCzynnosc(czynnosc.getId());
    }
    
    public String nazwaPracownik(Pracownik pracownik) {
        return pracownik.getImie()+" "+pracownik.getNazwisko()+" ("+pracownik.getStawka()+")";
    }

    public void setCzynnoscList(List<Czynnosc> czynnoscList) {
        this.czynnoscList = czynnoscList;
    }

    public int getIdZadanie() {
        return idZadanie;
    }

    public void setIdZadanie(int idZadanie) {
        this.idZadanie = idZadanie;
    }

    public String getTmpczynnoscNazwa() {
        return tmpczynnoscNazwa;
    }

    public void setTmpczynnoscNazwa(String tmpczynnoscNazwa) {
        this.tmpczynnoscNazwa = tmpczynnoscNazwa;
    }

   
    
    public void addCzynnosc(){
        Czynnosc tmpCzynnosc = new Czynnosc(Integer.SIZE, this.tmpczynnoscNazwa, getTmpczynnoscStawka(), getTmpczynnoscCzasTrwania());
        int rozmiar = czynnoscList.size();
        int ile = 0;
        for(Czynnosc czynnosc : czynnoscList) {
            if(czynnosc.getNazwa().equals(tmpczynnoscNazwa))
                ile++;
        }
        if(ile==0) {
            tmpCzynnosc.setPracownikList(this.getPracownicy().getTarget());
            db.addCzynnoscForZadanie(tmpCzynnosc,getPracownicy().getTarget(),idZadanie);
            czynnoscList.add(tmpCzynnosc);
            pracownicy = new DualListModel<>();
        }
    }

    public Double getTmpczynnoscStawka() {
        return tmpczynnoscStawka;
    }

    public void setTmpczynnoscStawka(Double tmpczynnoscStawka) {
        this.tmpczynnoscStawka = tmpczynnoscStawka;
    }

    public int getTmpczynnoscCzasTrwania() {
        return tmpczynnoscCzasTrwania;
    }

    public void setTmpczynnoscCzasTrwania(int tmpczynnoscCzasTrwania) {
        this.tmpczynnoscCzasTrwania = tmpczynnoscCzasTrwania;
    }

    public void setPracownicy(DualListModel<Pracownik> pracownicy) {
        this.pracownicy = pracownicy;
    }
    
    public void checkPracownik() {
        Czynnosc czynnosc = new Czynnosc(1,this.tmpczynnoscNazwa,this.tmpczynnoscStawka,this.tmpczynnoscCzasTrwania);
        List<Pracownik> prac = db.getPracownicyPosadajacyOdpowiednieNarzedzia(czynnosc);
        this.pracownicy.setSource(prac); 
    }
    
}
