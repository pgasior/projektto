package control;

import ejb.DataBean;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;


/**
 *
 * @author Piotrek
 */
@ManagedBean(name="AddPracownik")
@RequestScoped
public class AddPracownikBean {
    
    private static Logger logger = Logger.getLogger(".controll.AddPracownikBean");
    
    @EJB
    private DataBean db;
    
    private String imieNewP;
    private String nazwiskoNewP;
    private Double stawkaNewP;

    /**
     * Creates a new instance of AddBean
     */
    public AddPracownikBean() {
        logger.info("Konstruktor addPracownikBean");
        this.imieNewP="";
        this.nazwiskoNewP="";
    }

    /**
     * @return the imieNewP
     */
    public String getImieNewP() {
        return imieNewP;
    }

    /**
     * @param imieNewP the imieNewP to set
     */
    public void setImieNewP(String imieNewP) {
        this.imieNewP = imieNewP;
    }

    /**
     * @return the nazwiskoNewP
     */
    public String getNazwiskoNewP() {
        return nazwiskoNewP;
    }

    /**
     * @param nazwiskoNewP the nazwiskoNewP to set
     */
    public void setNazwiskoNewP(String nazwiskoNewP) {
        this.nazwiskoNewP = nazwiskoNewP;
    }
    
    public void addNewPracownik()
    {
        try {
            db.addPracownik(imieNewP, nazwiskoNewP, stawkaNewP);
            this.addMessage("Dodano pracownika: "+imieNewP+" "+nazwiskoNewP);
             this.imieNewP="";
        this.nazwiskoNewP="";
        } catch(Exception e) {
            logger.warning(e.getMessage());
            e.printStackTrace();
        }
    }
    
    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * @return the stawkaNewP
     */
    public Double getStawkaNewP() {
        return stawkaNewP;
    }

    /**
     * @param stawkaNewP the stawkaNewP to set
     */
    public void setStawkaNewP(Double stawkaNewP) {
        this.stawkaNewP = stawkaNewP;
    }
    
}
