/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import ejb.DataBean;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import model.Czynnosc;
import model.Pracownik;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Piotrek
 */
@ManagedBean(name = "addZadanie")
@ViewScoped
public class AddZadanieBean {

    private static Logger logger = Logger.getLogger(".controll.AddZadanieBean");
    
    private String Nazwa;
    private List<Czynnosc> czynnosci = new ArrayList<Czynnosc>();
    private DualListModel<Pracownik> pracownicy;
    private Date czasRozpoczecia;
    
    private String tmpczynnoscNazwa;
    private Double tmpczynnoscStawka;
    private int tmpczynnoscCzasTrwania;
    
    @EJB
    private DataBean db;

    /**
     * Creates a new instance of AddZadanieBean
     */
    public AddZadanieBean() {
    }

    public String getNazwa() {
        return Nazwa;
    }

    public void setNazwa(String Nazwa) {
        this.Nazwa = Nazwa;
    }

    public List<Czynnosc> getCzynnosci() {
        return czynnosci;
    }

    public void setCzynnosci(List<Czynnosc> czynnosci) {
        this.czynnosci = czynnosci;
    }



    public void setPracownicy(DualListModel<Pracownik> pracownicy) {
        this.pracownicy = pracownicy;
    }

    public String getTmpczynnoscNazwa() {
        return tmpczynnoscNazwa;
    }

    public void setTmpczynnoscNazwa(String tmpczynnoscNazwa) {
        this.tmpczynnoscNazwa = tmpczynnoscNazwa;
    }

    public Double getTmpczynnoscStawka() {
        return tmpczynnoscStawka;
    }

    public void setTmpczynnoscStawka(Double tmpczynnoscStawka) {
        this.tmpczynnoscStawka = tmpczynnoscStawka;
    }

    public int getTmpczynnoscCzasTrwania() {
        return tmpczynnoscCzasTrwania;
    }

    public void setTmpczynnoscCzasTrwania(int tmpczynnoscCzasTrwania) {
        this.tmpczynnoscCzasTrwania = tmpczynnoscCzasTrwania;
    }
    
    
    
    

    public Date getCzasRozpoczecia() {
        return czasRozpoczecia;
    }

    public void setCzasRozpoczecia(Date czasRozpoczecia) {
        this.czasRozpoczecia = czasRozpoczecia;
    }
    
    public void dodajZadanie() {
        db.addZadanie(this.Nazwa, this.czasRozpoczecia);
    }
    
    public void dodajZadanieKlientowi() {
        String username = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
        db.addZadanieKlientowi(this.Nazwa,this.czasRozpoczecia,username);
    }
    

    
}
