/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import ejb.DataBean;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import model.Pracownik;

/**
 *
 * @author Piotrek
 */
@ManagedBean(name = "deletePracownik")
@RequestScoped
public class DeletePracownikBean {

    private static Logger logger = Logger.getLogger(".controll.DeletePracownikBean");

    @EJB
    private DataBean db;

    /**
     * Creates a new instance of DeleteBean
     */
    public DeletePracownikBean() {
    }

    public void deletePracownik(Pracownik pracownik) {
        try {
            db.deletePracownik(pracownik);
        } catch (Exception e) {
            logger.warning(e.getMessage());
            e.printStackTrace();
        }
    }

}
