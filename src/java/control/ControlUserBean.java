/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import ejb.DataBean;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import model.Users;

/**
 *
 * @author Piotrek
 */
@ManagedBean(name = "controlUser")
@RequestScoped
public class ControlUserBean {

    @EJB
    private DataBean db;

    private List<Users> userList;
    private String newName = "";
    private String newPassword = "";
    private List<String> groups;

    private String selectedGroup = "";
    private String selectedUsername = "";
    private String selectedPassword = "";

    public ControlUserBean() {
    }

    public List<Users> getUserList() {
        this.userList = db.getUsersList();
        return userList;
    }

    public void setUserList(List<Users> userList) {
        this.userList = userList;
    }

    public String getUserGroup(String name) {
        return db.getUserGroup(name);
    }

    public String getNewName() {
        return newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public List<String> getGroups() {
        if (groups == null) {
            groups = new ArrayList<>();
            groups.add("admin");
            groups.add("uzytkownik");
            groups.add("klient");
        }
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    public String getSelectedGroup() {
        return selectedGroup;
    }

    public void setSelectedGroup(String selectedGroup) {
        this.selectedGroup = selectedGroup;
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public String getSelectedUsername() {
        return selectedUsername;
    }

    public void setSelectedUsername(String selectedUsername) {
        this.selectedUsername = selectedUsername;
    }

    public String getSelectedPassword() {
        return selectedPassword;
    }

    public void setSelectedPassword(String selectedPassword) {
        this.selectedPassword = selectedPassword;
    }
    
    public void deleteUser(String username) {
        db.deleteUser(username);
    }

    public void addUser() {
        if (this.selectedUsername.equals("")) {
            this.addMessage("Nie podano loginu");
            return;
        } else if (this.selectedPassword.equals("")) {
            this.addMessage("Nie podano Hasla");
            return;
        } else if (this.selectedGroup.equals("")) {
            this.addMessage("Nie podano grupy");
            return;
        } else {
            for (Users u : this.userList) {
                if (u.getUsername().equals(this.selectedUsername)) {
                    this.addMessage("Nazwa zajeta");
                    return;
                }
            }
        }
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            
            md.update(this.selectedPassword.getBytes("UTF-8")); // Change this to "UTF-16" if needed
            byte[] digest = md.digest();
            BigInteger bigInt = new BigInteger(1, digest);
            this.selectedPassword = bigInt.toString(16);



        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
        }
        db.addUser(selectedUsername, selectedPassword, selectedGroup);
        this.selectedUsername="";
        this.selectedPassword="";
        

    }
    
    public String username() {
        return FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
    }

}
