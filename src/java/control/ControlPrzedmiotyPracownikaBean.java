/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import ejb.DataBean;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import model.Pracownik;
import model.Przedmiot;

/**
 *
 * @author Piotrek
 */
@ManagedBean(name = "controlPrzedmiotyPracownika")
@RequestScoped
public class ControlPrzedmiotyPracownikaBean {

    /**
     * Creates a new instance of ControlPrzedmiotyPracownikaBean
     */
    
    private static Logger logger = Logger.getLogger(".control.ControlPrzedmiotyPracownikaBean");
    @ManagedProperty(value="#{param.pracid}")
    private int pracownikID;
    
    private Pracownik pracownik;
    
    @EJB
    private DataBean db;
    
    private List<Przedmiot> PrzedmiotyPracownika = null;
    
   
    public ControlPrzedmiotyPracownikaBean() {
        
    }
    
    public String imiePracownika() {
        return pracownik.getImie() + " " + pracownik.getNazwisko();
    }
    
    public void loadData() {
        this.pracownik = db.getPracownikById(this.pracownikID);
    }
    
    public List<Przedmiot> getPrzedmiotyPracownika() {
        this.loadData();
        return (List<Przedmiot>) this.pracownik.getPrzedmiotList();
    }
    
    public void deletePrzedmiotPracownika(Przedmiot przedmiot) {
        //przedmiot.setIdPracownik(null);
        //przedmiot.setIdPracownik(null);
        //this.pracownik.getPrzedmiotCollection().remove(przedmiot);
        //db.deletePrzedmiotPracownika(przedmiot, this.pracownik);
        //List<Przedmioty> przedmioty = this.pracownik.getPrzedmiotCollection().remove(przedmiot);
        //przedmiot.setNazwa("USUWANY");
        db.deletePrzedmiotPracownika(przedmiot,this.pracownik);
    }

    /**
     * @return the pracownikID
     */
    public int getPracownikID() {
        //logger.info("getpracownikID");
        return pracownikID;
    }

    /**
     * @param pracownikID the pracownikID to set
     */
    public void setPracownikID(int pracownikID) {
        this.pracownikID = pracownikID;
    }


    
}
