package control;


import ejb.DataBean;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import model.Pracownik;

/**
 *
 * @author Piotrek
 */
@ManagedBean(name = "Controler1")
@RequestScoped
public class ControlPracownikBean implements Serializable{
    
    private static Logger logger = Logger.getLogger(".control.ControlPracownikBean");
    
    @EJB
    private DataBean db;
    
    private String hello;
    
    private List<Pracownik> Zatrudnieni;

    /**
     * Creates a new instance of ControlBean
     */
    public ControlPracownikBean() {
    }

    /**
     * @return the hello
     */
    public String getHello() {
        //return hello;
        return "Hello TO";
    }

    /**
     * @param hello the hello to set
     */
    public void setHello(String hello) {
        this.hello = hello;
    }

    /**
     * @return the zatrudnieni
     */
    public List<Pracownik> getZatrudnieni() {
        try{
            this.Zatrudnieni = db.getPracownicy();
        } catch(Exception e) {
            logger.warning(("nieudalo sie pobrac pracownikow"));
            e.printStackTrace();
        }
        return Zatrudnieni;
    }
    
}
