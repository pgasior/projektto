/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import ejb.DataBean;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Piotrek
 */
@ManagedBean(name = "addZastosowanie")
@RequestScoped
public class AddZastosowanieBean {
    
    private static Logger logger = Logger.getLogger(".controll.AddZastosowanieBean");
    
    private String nazwaNewZastosowanie;

    @EJB
    private DataBean db;

    /**
     * Creates a new instance of AddZastosowanieBean
     */
    public AddZastosowanieBean() {
    }

    /**
     * @return the nazwaNewZastosowanie
     */
    public String getNazwaNewZastosowanie() {
        return nazwaNewZastosowanie;
    }

    /**
     * @param nazwaNewZastosowanie the nazwaNewZastosowanie to set
     */
    public void setNazwaNewZastosowanie(String nazwaNewZastosowanie) {
        this.nazwaNewZastosowanie = nazwaNewZastosowanie;
    }
    
    public void addNewZastosowanie() {
        try {
            db.addZastosowanie(nazwaNewZastosowanie);
            this.addMessage("Dodano przedmiot: " + nazwaNewZastosowanie);
            this.nazwaNewZastosowanie = "";

        } catch (Exception e) {
            logger.warning(e.getMessage());
            e.printStackTrace();
        }
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
}
