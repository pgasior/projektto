/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJBException;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Czynnosc;
import model.Groups;
import model.Pracownik;
import model.Przedmiot;
import model.Users;
import model.Wiadomosc;
import model.Zadanie;
import model.Zastosowanie;
import org.jboss.logging.Logger;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Piotrek
 */
@Stateful
public class DataBean {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    private static Logger logger = Logger.getLogger(".ejb.DataBean");

    @PersistenceContext
    private EntityManager em;

    public List<Pracownik> getPracownicy() {
        List<Pracownik> pracownicy = null;
        try {
            pracownicy = (List<Pracownik>) em.createNamedQuery("Pracownik.findAll").getResultList();
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
        return pracownicy;
    }

    public List<Zastosowanie> getZastosowania() {
        List<Zastosowanie> zastosowania = null;
        try {
            zastosowania = (List<Zastosowanie>) em.createNamedQuery("Zastosowanie.findAll").getResultList();
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
        return zastosowania;
    }

    public Pracownik getPracownikById(int id) {
        Pracownik pracownik = null;
        try {
            pracownik = (Pracownik) em.createNamedQuery("Pracownik.findById")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
        return pracownik;
    }

    public Przedmiot getPrzedmiotById(int id) {
        Przedmiot przedmiot = null;
        try {
            przedmiot = em.find(Przedmiot.class, id);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
        return przedmiot;
    }

    public void addPracownik(String imie, String nazwisko, double stawka) {
        try {
            Pracownik dodawany = new Pracownik(Integer.valueOf(1), imie, nazwisko, stawka);
            logger.info("Dodaje użytkownika: " + imie + " " + nazwisko);
            em.persist(dodawany);
            logger.info("Dodamo użytkownika: " + imie + " " + nazwisko);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public void addZastosowanie(String nazwa) {
        try {
            Zastosowanie dodawane = new Zastosowanie(Integer.valueOf(1), nazwa);
            logger.info("Dodaje zastosowanie: " + nazwa);
            em.persist(dodawane);
            logger.info("Dodalem zastosowanie: " + nazwa);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public void deletePracownik(Pracownik pracownik) {
        try {
            logger.info("Usuwam uzytkownika: " + pracownik.getImie() + " " + pracownik.getNazwisko());
            Pracownik doUsuniecia = em.find(Pracownik.class, pracownik.getId());
            em.remove(doUsuniecia);
            logger.info("Usunalem uzytkownika: " + pracownik.getImie() + " " + pracownik.getNazwisko());
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public void deleteZastosowanie(Zastosowanie zastosowanie) {
        try {
            logger.info("Usuwam zastosowanie: " + zastosowanie.getNazwa());
            Zastosowanie doUsuniecia = em.find(Zastosowanie.class, zastosowanie.getId());
            em.remove(doUsuniecia);
            logger.info("Usunalem zastosowanie: " + zastosowanie.getNazwa());
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public List<Przedmiot> getPrzedmioty() {
        List<Przedmiot> przedmioty = null;
        try {
            przedmioty = (List<Przedmiot>) em.createNamedQuery("Przedmiot.findAll").getResultList();
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
        return przedmioty;
    }

    public void addPrzedmiot(String nazwa) {
        try {
            Przedmiot dodawany = new Przedmiot(Integer.valueOf(1), nazwa);
            logger.info("Dodaje przedmiot: " + nazwa);
            em.persist(dodawany);
            logger.info("Dodalem przedmiot: " + nazwa);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public void deletePrzedmiot(Przedmiot przedmiot) {
        try {
            logger.info("Usuwam przedmiot: " + przedmiot.getNazwa());
            Przedmiot doUsuniecia = em.getReference(Przedmiot.class, przedmiot.getId());
            em.remove(doUsuniecia);
            logger.info("Usunalem przedmiot: " + przedmiot.getNazwa());
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public List<Przedmiot> getWolnePrzedmioty() {
        List<Przedmiot> przedmioty = null;
        try {
            //przedmioty = (List<Przedmiot>) em.createNamedQuery("Przedmiot.findAll").getResultList();
            Query q = em.createQuery("SELECT p FROM Przedmiot p where p.idPracownik IS NULL");
            przedmioty = (List<Przedmiot>) q.getResultList();
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
        return przedmioty;
    }

    public List<Przedmiot> getPrzedmiotyPracownika(int id) {
        List<Przedmiot> przedmioty = null;
        Pracownik pracownik = null;
        try {
            //przedmioty = (List<Przedmiot>) em.createNamedQuery("Przedmiot.findAll").getResultList();
            pracownik = (Pracownik) em.createNamedQuery("Pracownik.findById")
                    .setParameter("id", id)
                    .getSingleResult();
            przedmioty = (List<Przedmiot>) pracownik.getPrzedmiotList();

        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
        return przedmioty;
    }

    public void deletePrzedmiotPracownika(Przedmiot przedmiot, Pracownik pracownik) {
        try {
            Przedmiot doUsunieciaPrzedmiot = em.find(Przedmiot.class, przedmiot.getId());
            //doUsunieciaPrzedmiot.getIdPracownik();
            Pracownik doZmiany = em.find(Pracownik.class, pracownik.getId());
            //doUsunieciaPrzedmiot.setIdPracownik(null);
            doZmiany.removePrzedmiot(doUsunieciaPrzedmiot);
            //doUsunieciaPrzedmiot.setIdPracownik(null);
            //em.refresh(doZmiany);
            //doZmiany.getPrzedmiotList().remove(doUsunieciaPrzedmiot);
            //em.flush();

            //List<Przedmiot> przedmioty = (List<Przedmiot>)doZmiany.getPrzedmiotCollection();
            //Przedmiot p2 = przedmioty.get(0);
            //doZmiany.getPrzedmiotCollection().remove(doUsunieciaPrzedmiot);
            //doUsunieciaPrzedmiot.getIdPracownik();
            //doUsunieciaPrzedmiot.getIdPracownik();
            //doUsunieciaPrzedmiot.setIdPracownik(null);
            //em.flush();
            //em.persist(doZmiany);
            //em.merge(doUsunieciaPrzedmiot);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public void addZastosowaniaPrzedmiotu(int przedmiotID, DualListModel<Zastosowanie> zastosowania) {
        Przedmiot przedmiot;
        try {
            przedmiot = em.find(Przedmiot.class, przedmiotID);
            List<Zastosowanie> doDodania = zastosowania.getTarget();
            List<Zastosowanie> doUsuniecia = zastosowania.getSource();

            for (Zastosowanie z : doDodania) {
                z = em.find(Zastosowanie.class, z.getId());
                if (!przedmiot.getZastosowanieList().contains(z)) {
                    przedmiot.getZastosowanieList().add(z);
                    z.getPrzedmiotList().add(przedmiot);
                }

            }

            for (Zastosowanie z : doUsuniecia) {
                z = em.find(Zastosowanie.class, z.getId());
                przedmiot.getZastosowanieList().remove(z);
                z.getPrzedmiotList().remove(przedmiot);
            }
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public void addPrzedmiotForPracownik(Pracownik pracownik, DualListModel<Przedmiot> przedmioty) {
        try {
            pracownik = em.find(Pracownik.class, pracownik.getId());
            logger.info(pracownik.getImie());
            logger.info("dodaje dla pracownika: " + pracownik.getImie() + " " + pracownik.getNazwisko());
            List<Przedmiot> doDodania = przedmioty.getTarget();
            List<Przedmiot> doDodaniaReferenced = new ArrayList<>();
            List<Przedmiot> doUsuniecia = przedmioty.getSource();

            for (Przedmiot el : doUsuniecia) {
                if (el.getIdPracownik() != null) {
                    Przedmiot tmp = em.getReference(Przedmiot.class, el.getId());
                    tmp.setIdPracownik(null);
                    em.merge(tmp);
                }
            }
            for (Przedmiot el : doDodania) {
                Przedmiot tmp = em.getReference(Przedmiot.class, el.getId());
                tmp.setIdPracownik(pracownik);
                em.merge(tmp);
                doDodaniaReferenced.add(tmp);
            }
            pracownik.setPrzedmiotList(doDodaniaReferenced);
            em.merge(pracownik);

        } catch (Exception e) {
            throw new EJBException(e.getMessage());

        }
    }

    public List<Zastosowanie> getZastosowaniaPrzedmiotu(int id) {
        List<Zastosowanie> zastosowania = null;
        Przedmiot przedmiot = null;
        try {
            przedmiot = em.find(Przedmiot.class, id);
            zastosowania = przedmiot.getZastosowanieList();
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }

        return zastosowania;
    }

    //TODO: delete przedmiot from pracownik
    public void removeZastosowaniePrzedmiotu(int zastosowanieID, int przedmiotID) {
        Zastosowanie zastosowanie = null;
        Przedmiot przedmiot = null;
        try {
            zastosowanie = em.find(Zastosowanie.class, zastosowanieID);
            przedmiot = em.find(Przedmiot.class, przedmiotID);
            zastosowanie.getPrzedmiotList().remove(przedmiot);
            przedmiot.getZastosowanieList().remove(zastosowanie);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public List<Zastosowanie> getNieprzypisaneZastosowaniaPrzedmiotu(int przedmiotID) {
        List<Zastosowanie> zastosowania;
        try {
            Query q = em.createNativeQuery("SELECT z.* FROM Zastosowanie z where z.id not in "
                    + "(select pz.ID_ZASTOSOWANIE FROM przedmiot_zastosowanie pz where pz.ID_PRZEDMIOT=?1)", Zastosowanie.class);
            // Query q = em.createQuery("SELECT DISTINCT z FROM Zastosowanie z WHERE p.id=:p_id AND z NOT IN (p.zastosowanieList)", Zastosowanie.class);
            q.setParameter(1, przedmiotID);
            //q.setParameter("p_id", przedmiotID);
            zastosowania = (List< Zastosowanie>) q.getResultList();
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
        return zastosowania;
    }

    public List<Pracownik> getPracownicyPosadajacyOdpowiednieNarzedzia(Czynnosc czynnosc) {
        List<Pracownik> pracownicy = new ArrayList<>();
        
        try {

            List<Pracownik> wszyscy = this.getPracownicy();
            for (Pracownik pracownik : wszyscy) {
                for (Przedmiot przedmiot : pracownik.getPrzedmiotList()) {
                    for (Zastosowanie zastosowanie : przedmiot.getZastosowanieList()) {
                        if (czynnosc.getNazwa().equals(zastosowanie.getNazwa())) {
                            pracownicy.add(pracownik);
                            logger.info(pracownik.getImie());
                        }
                    }
                }
                
            }
            
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
        return pracownicy;
    }

    public void addZadanie(String nazwa, Date czas_rozpoczecia) {
        try {
            Zadanie zadanie = new Zadanie(1, nazwa, czas_rozpoczecia);
            zadanie.setStatus(0);
            em.persist(zadanie);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    private void addCzynnosci(List<Czynnosc> czynnosci, Zadanie zadanie) {
        try {
            for (Czynnosc c : czynnosci) {
                em.persist(c);
                c.setIdZadanie(zadanie);
                zadanie.getCzynnoscList().add(c);
            }
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public List<Zadanie> getZadania() {
        List<Zadanie> zadania;
        try {
            zadania = (List<Zadanie>) em.createNamedQuery("Zadanie.findAll").getResultList();
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
        return zadania;
    }

    public void removeZadanie(Integer id) {
        try {
            Zadanie doUsuniecia = em.find(Zadanie.class, id);
            em.remove(doUsuniecia);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public List<Czynnosc> getCzynnoscForZadanie(int idZadanie) {
        try {
            Zadanie zadanie = em.find(Zadanie.class, idZadanie);
            return zadanie.getCzynnoscList();
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
        
    }

    public void addCzynnoscForZadanie(Czynnosc tmpCzynnosc, List<Pracownik> pracownicy, int idZadanie) {
        try {
            em.persist(tmpCzynnosc);
            em.flush();
            em.refresh(tmpCzynnosc);
            
            for(Pracownik p : pracownicy) {
                p=em.find(Pracownik.class, p.getId());
                p.getCzynnoscList().add(tmpCzynnosc);
                tmpCzynnosc.getPracownikList().add(p);
            }
            Zadanie zadanie = em.find(Zadanie.class,idZadanie);
            zadanie.getCzynnoscList().add(tmpCzynnosc);
            tmpCzynnosc.setIdZadanie(zadanie);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public void removeCzynnosc(Integer id) {
        try {
            Czynnosc czynnosc = em.find(Czynnosc.class, id);
            for(Pracownik p : czynnosc.getPracownikList()) {
                p.getCzynnoscList().remove(czynnosc);
            }
            czynnosc.setPracownikList(null);
            czynnosc.getIdZadanie().getCzynnoscList().remove(czynnosc);
            czynnosc.setIdZadanie(null);
            em.remove(czynnosc);
            
        }catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public List<Users> getUsersList() {
       List<Users> userslist;
       try {
           userslist = em.createNamedQuery("Users.findAll", Users.class).getResultList();
       }catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
       return userslist;
    }

    public String getUserGroup(String name) {
        String groupname;
        try {
            Groups group = em.find(Groups.class, name);
            groupname = group.getGroupname();
        }catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
        return groupname;
    }

    public void addUser(String selectedUsername, String selectedPassword, String selectedGroup) {
        try {
            Users user = new Users(selectedUsername, selectedPassword);
            em.persist(user);
            Groups group = new Groups(selectedUsername, selectedGroup);
            em.persist(group);
        }catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public void deleteUser(String username) {
        try {
            Users user = em.find(Users.class, username);
            em.remove(user);
            Groups group = em.find(Groups.class, username);
            em.remove(group);
        }catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public List<Zadanie> getZadaniaKlienta(String username) {
        List<Zadanie> zad;
        try {
            zad = em.createNamedQuery("Zadanie.findByKlient", Zadanie.class).setParameter("username", username).getResultList();
        }catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
        return zad;
    }

    public void addZadanieKlientowi(String Nazwa, Date czasRozpoczecia, String username) {
        try {
            Zadanie zadanie = new Zadanie(1,Nazwa,czasRozpoczecia);
            zadanie.setStatus(0);
            em.persist(zadanie);
            em.flush();
            em.refresh(zadanie);
            
            Users user = em.find(Users.class, username);
            zadanie.setKlient(user);
            user.getZadanieList().add(zadanie);
        }catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public List<Users> getUsersFromGroup(String klient) {
        try {
            List<Groups> g = em.createNamedQuery("Groups.findByGroupname", Groups.class).setParameter("groupname", klient).getResultList();
            List<Users> u = new ArrayList<>();
            for(Groups gr : g) {
                Users us = em.find(Users.class, gr.getUsername());
                u.add(us);
            }
            return u;
        }catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public List<Wiadomosc> getOdebraneWiadomosci(String name) {
        try {
            Query q = em.createQuery("select w from Wiadomosc w where w.odbiorca.username= :odbiorca ORDER BY w.dataWyslania DESC", Wiadomosc.class);           
            List<Wiadomosc> w = q.setParameter("odbiorca", name).getResultList();
            return w;
        }catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
        
    }

    public void wyslijWiadomosc(String wiadomosc, String tytul, String odbiorca, String nadawca) {
        try {
            Wiadomosc w = new Wiadomosc(1);
            Users n = em.find(Users.class, nadawca);
            Users o = em.find(Users.class, odbiorca);
            w.setTresc(wiadomosc);
            w.setTytul(tytul);   
            w.setDataWyslania(new Date());
            w.setPrzeczytana(0);
            em.persist(w);
            em.flush();
            em.refresh(w);
            w.setNadawca(n);
            n.getWyslaneList().add(w);
            
            w.setOdbiorca(o);
            o.getOdebraneList().add(w);

            
        }catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public void setWiadomoscPrzeczytana(Integer id) {
        try {
            Wiadomosc w = em.find(Wiadomosc.class, id);
            w.setPrzeczytana(1);
        }catch(Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public void ustawStatusZadania(Integer id, int i) {
        try {
            Zadanie z = em.find(Zadanie.class, id);
            z.setStatus(i);
            em.flush();
        }catch(Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
}
