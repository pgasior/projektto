/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import control.ControlZadanieBean;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.faces.bean.ManagedProperty;
import model.Zadanie;

/**
 *
 * @author Piotrek
 */
@Singleton
public class Harmonogram {
    
    @EJB
    private DataBean db;
    

    @Schedule(dayOfWeek = "Mon-Fri", month = "*", hour = "*", dayOfMonth = "*", year = "*", minute = "*", second = "*/20", persistent = false)
    
    public void myTimer() {
        System.out.println("Timer");
        List<Zadanie> zadania = db.getZadania();
        Date teraz;
        Calendar cal = Calendar.getInstance();
        ControlZadanieBean czb = new ControlZadanieBean();
        for(Zadanie z : zadania) {
            teraz = new Date();
            System.out.println(teraz);
            if(teraz.before(z.getCzasRozpoczecia())) {
                System.out.println("Status przed");
                db.ustawStatusZadania(z.getId(),0);
                //z.setStatus(0);
            } else if(teraz.after(z.getCzasRozpoczecia()) && teraz.before(czb.dataZakonczeniaZadania(z))) {
                System.out.println("Status w trakcie");
                db.ustawStatusZadania(z.getId(), 1);
               // z.setStatus(1);
            } else if(teraz.after(czb.dataZakonczeniaZadania(z))) {
                System.out.println("Status po");
                db.ustawStatusZadania(z.getId(), 2);
                //z.setStatus(2);
            }
        }
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
